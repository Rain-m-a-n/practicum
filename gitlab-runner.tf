resource "yandex_compute_instance" "gitlab-runner" {
  name        = "gitlab-runner"
  hostname    = "gitlab-runner"
  platform_id = "standard-v1"
  zone        = var.default_zone

  resources {
    cores         = 2
    memory        = 2
    core_fraction = 5
  }

  boot_disk {
    initialize_params {
      name     = "ubuntu-22-04"
      size     = 15
      type     = "network-hdd"
      image_id = "fd828qsqv1jtq2qec4m5"
    }
  }

  network_interface {
    subnet_id = "e2lqvse8c94h125iu3qb"
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}
