resource "yandex_iam_service_account_static_access_key" "sa-static-key" {
  service_account_id = var.service_account_id
  description        = "static access key for object storage"
}

resource "yandex_storage_bucket" "test" {
  access_key            = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key            = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  bucket                = "bsv27-state"
  max_size              = 10000000
  default_storage_class = "STANDARD"
  anonymous_access_flags {
    read = true
    list = true
    config_read = false
  }
}