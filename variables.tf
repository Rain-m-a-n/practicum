variable "default_zone" {
  type = string
  default = "ru-central1-b"
}

variable "token" {
  type = string
}

variable "cloud_id" {
  type = string
}

variable "folder_id" {
  type = string
}

variable "service_account_id" {
  type = string
  default = "aje2p2bkg6kjrdu29dcg"
}