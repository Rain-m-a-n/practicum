terraform {
  backend "s3" {
    region         = "ru-central1"
    bucket         = "bsv2004"
    key            = "terraform.tfstate"

    dynamodb_table = "state-lock-table"

    endpoints = {
      s3       = "https://storage.yandexcloud.net",
      dynamodb = "https://docapi.serverless.yandexcloud.net/ru-central1/b1gcqdohje6g5rkr6lsh/etn2vu1kg1jjfr65fdtb"
    }

    skip_credentials_validation = true
    skip_region_validation      = true
    skip_requesting_account_id  = true
    skip_s3_checksum            = true
  }
}
